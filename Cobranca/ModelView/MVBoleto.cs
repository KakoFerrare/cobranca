﻿using ClassModel.Model;
using Cobranca.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.ModelView
{
    public class MVBoleto
    {
        public ContaCorrente contaCorrente { get; set; }
        public Boleto boleto { get; set; }

        public List<Boleto> Lista_Boleto { get; set; }
        public List<Item> listaItem { get; set; }
        public List<Item> listaEspecie { get; set; }

        public MVBoleto()
        {
            boleto = new Boleto();
            contaCorrente = new ContaCorrente();
            listaEspecie = new List<Item>();
            listaItem = new List<Item>();
            listaItem.AddRange(new CN.CNItem().Listar(60));
            listaEspecie.AddRange(new CN.CNItem().Listar(61));
        }

        public List<Boleto> Lista()
        {
            Lista_Boleto = new CN.CNBoleto().Listar();
            return Lista_Boleto;
        }
    }


}
