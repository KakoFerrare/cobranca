﻿using Cobranca.Model;
using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cobranca.DAO
{
    class DAOItem
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "COB_LISTAITEM";
        private FbConnection conn = new FbConnection();
        private string connection = Properties.Settings.Default.Connection;

        public List<Item> Listar(int? idGrupo = null)
        {
            strQuery = new StringBuilder();
            List<Item> ListaItem = new List<Item>();
            strQuery.Append(" Select idgrupo, idlista, descricao, idusuario_inc, dt_inc, idusuario_alt, dt_alt from " + table);
            if (idGrupo != null)
            {
                strQuery.Append(" WHERE idgrupo = @IdGrupo");
            }
            using (conn = new FbConnection(connection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.Add("@IdGrupo", idGrupo);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Item item = new Item();
                                foreach (PropertyInfo propertyInfo in item.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error")
                                        propertyInfo.SetValue(item, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                                ListaItem.Add(item);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    ListaItem = new List<Item>();
                    MessageBox.Show("Erro ao carregar itens do boleto: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return ListaItem;
        }
    }
}
