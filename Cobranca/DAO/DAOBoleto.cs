﻿using Cobranca.Model;
using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cobranca.DAO
{
    class DAOBoleto
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "COB_BOLETO";
        private FbConnection conn = new FbConnection();
        private string connection = Properties.Settings.Default.Connection;

        public List<Boleto> Listar(string idConta = null)
        {
            strQuery = new StringBuilder();
            List<Boleto> Lista = new List<Boleto>();
            strQuery.Append(" Select first(5) idconta, nossonr, prefixo, dvnossonr, idbordero, controlecliente, parcela, carteira, cod_remessa, nrdoc, vencimento, valor, especie, aceite, data, instr1, instr2, mora, ").
                Append(" multa, abatimento, idsacado, sac_idconta, sacador, sacador_idcpf, dtpagamento, valorpago, agencia_rec, banco_rec, tipobaixa, tpvencimento, codbarras, linhadigitavel, envio, sac_tipo, sac_nome, ").
                Append(" sac_end, sac_bai, sac_cep, sac_cid, sac_uf, sac_idcpf, mensagem1, mensagem2, mensagem3, mensagem4, mensagem5, desc_ate, vlr_desc, prazo, banco, enviado_itau, agencia_bol, idusuario_inc, dt_inc, idusuario_alt, dt_alt from " + table);
            if (idConta != null)
            {
                strQuery.Append(" WHERE idconta = @IdConta");
            }
            using (conn = new FbConnection(connection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.Add("@IdConta", idConta);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Boleto boleto = new Boleto();
                                foreach (PropertyInfo propertyInfo in boleto.GetType().GetProperties())
                                {

                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(boleto, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }

                                Lista.Add(boleto);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Lista = new List<Boleto>();
                    MessageBox.Show("Erro ao carregar itens do boleto: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return Lista;
        }

        public Boleto Carregar(string idConta, string nossoNumero, string prefxo)
        {
            strQuery = new StringBuilder();
            Boleto boleto = new Boleto();
            strQuery.Append(" Select idconta, nossonr, prefixo, dvnossonr, idbordero, controlecliente, parcela, carteira, cod_remessa, nrdoc, vencimento, valor, especie, aceite, data, instr1, instr2, mora, ").
                Append(" multa, abatimento, idsacado, sac_idconta, sacador, sacador_idcpf, dtpagamento, valorpago, agencia_rec, banco_rec, tipobaixa, tpvencimento, codbarras, linhadigitavel, envio, sac_tipo, sac_nome, ").
                Append(" sac_end, sac_bai, sac_cep, sac_cid, sac_uf, sac_idcpf, mensagem1, mensagem2, mensagem3, mensagem4, mensagem5, desc_ate, vlr_desc, prazo, banco, enviado_itau, agencia_bol, idusuario_inc, dt_inc, idusuario_alt, dt_alt from " + table);

            strQuery.Append(" WHERE idconta = @IdConta  AND nossonr = @NossoNumero  AND prefixo = @Prefixo");
            using (conn = new FbConnection(connection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.Add("@IdConta", idConta);
                        cmd.Parameters.Add("@NossoNumero", nossoNumero);
                        cmd.Parameters.Add("@Prefixo", prefxo);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                foreach (PropertyInfo propertyInfo in boleto.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(boleto, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    boleto = new Boleto();
                    MessageBox.Show("Erro ao carregar itens do boleto: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return boleto;
        }
    }

}
