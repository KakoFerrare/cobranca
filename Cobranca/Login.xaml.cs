﻿using Cobranca.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cobranca
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void LoginUser(object sender, RoutedEventArgs e)
        {
            string[] user = new Funcoes.CN.Login().LogIn(txtLogin.Text, pwdSenha.Password, Properties.Settings.Default.Connection);
            if(user[0] != null && user[3] != "0")
            {
                PersistUser(user);
                MainWindow main = new MainWindow();
                this.Close();
                main.Show();
            }
            else
            {
                lblOutPutMessage.Content = user[5];
            }

           
        }

        private void PersistUser(string[] user)
        {
            Properties.Settings.Default.UserId = Convert.ToInt32(user[0]);
            Properties.Settings.Default.UserName = user[1];
            Properties.Settings.Default.Ativo = Convert.ToBoolean(Convert.ToInt32(user[3]));
            Properties.Settings.Default.Nivel = Convert.ToInt32(user[2]);
            Properties.Settings.Default.UserPa = Convert.ToInt32(user[4]);
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
