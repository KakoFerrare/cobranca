﻿using Cobranca.DAO;
using Cobranca.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.CN
{
    class CNBoleto
    {
        DAOBoleto DAOBoleto = new DAOBoleto();
        public List<Boleto> Listar(string idConta = null)
        {
            return this.DAOBoleto.Listar(idConta);
        }

        public Boleto Listar(string idConta, string nossoNumero, string prefixo)
        {
            return this.DAOBoleto.Carregar(idConta, nossoNumero, prefixo);
        }
    }
}
