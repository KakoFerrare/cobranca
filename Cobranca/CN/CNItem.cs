﻿using Cobranca.DAO;
using Cobranca.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.CN
{

    public class CNItem
    {
        DAOItem DAOItem = new DAOItem();
        public List<Item> Listar(int? idGrupo = null)
        {
            return this.DAOItem.Listar(idGrupo);
        }
    }
}
