﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace Cobranca
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()

        {

        }

        protected override void OnStartup(StartupEventArgs e)

        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR"); ;
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pt-BR"); ;
            FrameworkElement.LanguageProperty.OverrideMetadata(
              typeof(FrameworkElement),
              new FrameworkPropertyMetadata(
                    XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
            base.OnStartup(e);
        }
    }
}