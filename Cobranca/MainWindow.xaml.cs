﻿using Cobranca.ModelView;
using Cobranca.View.Cadastramento;
using Cobranca.View.Relatorios;
using System;
using System.Windows;
using System.Windows.Threading;

namespace Cobranca
{
    public partial class MainWindow : Window
    {
        DispatcherTimer t;
        DateTime start;
        public MainWindow()
        {
            InitializeComponent();
            this.LabelUserName.Content = "Usuário: " + Properties.Settings.Default.UserName;
            this.LabelUserPa.Content = "PA: " + Properties.Settings.Default.UserPa;
            t = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 50), DispatcherPriority.Background,
              t_Tick, Dispatcher.CurrentDispatcher); t.IsEnabled = true;
            start = DateTime.Now;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Relatorio relatorio = new Relatorio();
            this.ContentTitle.Content = "Relatório";
            this.MainContent.Content = relatorio;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Boleto boleto = new Boleto(new MVBoleto());
            this.ContentTitle.Content = "Cadastro de Boleto";
            this.MainContent.Content = boleto;
        }

        private void MenuItem_Logout(object sender, RoutedEventArgs e)
        {
            Logout();
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        private void t_Tick(object sender, EventArgs e)
        {
            TimerDisplay.Content = Convert.ToString(DateTime.Now);
        }

        private void Logout()
        {
            Properties.Settings.Default.UserId = 0;
            Properties.Settings.Default.UserName = String.Empty;
            Properties.Settings.Default.Ativo = false;
            Properties.Settings.Default.Nivel = 0;
            Properties.Settings.Default.UserPa = 0;
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            Listar lista = new Listar(new MVBoleto().Lista());
             new MVBoleto();
        }
    }
}
