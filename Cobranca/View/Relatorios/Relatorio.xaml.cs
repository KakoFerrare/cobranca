﻿using Cobranca.CN;
using Cobranca.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cobranca.View.Relatorios
{
   
    public partial class Relatorio : UserControl
    {
        //private string[] values;
        public Relatorio()
        {
            InitializeComponent();
        }

        private void PesquisarConta(object sender, RoutedEventArgs e)
        {
            Button botao = (sender as Button);
            String title = botao.Tag.ToString();
            Funcoes.CN.Pesquisa FuncoesPesquisa = new Funcoes.CN.Pesquisa();
            FuncoesPesquisa.ContaCorrente();
            Pesquisa pesquisa = new Pesquisa(title, FuncoesPesquisa);
            pesquisa.ShowDialog();
            if (!Object.ReferenceEquals(pesquisa.values, null))
            {
                //this.values = values;
                this.NumeroConta.Text = new Funcoes.CN.FuncoesGerais().FormatConta(pesquisa.values[0]);
                this.NomeConta.Text = pesquisa.values[2];
            }

        }

        private void PesquisarNossoNumero(object sender, RoutedEventArgs e)
        {
            Button botao = (sender as Button);
            String title = botao.Tag.ToString();
            Funcoes.CN.Pesquisa FuncoesPesquisa = new Funcoes.CN.Pesquisa();
            FuncoesPesquisa.NossoNumero();
            Pesquisa pesquisa = new Pesquisa(title, FuncoesPesquisa);
            pesquisa.ShowDialog();
            if (!Object.ReferenceEquals(pesquisa.values, null))
            {
                //this.values = values;
                //this.Conta.Text = new Funcoes.CN.FuncoesGerais().FormatConta(pesquisa.values[0]);
                //this.Nome.Content = pesquisa.values[2];
            }

        }
    }
}
