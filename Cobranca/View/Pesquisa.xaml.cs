﻿using Cobranca.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Cobranca.View
{
    public partial class Pesquisa : Window
    {
        private Funcoes.CN.Pesquisa FuncoesPesquisa;
        public string[] values = null;

        public Pesquisa(string title = "Pesquisa", Funcoes.CN.Pesquisa Pesquisa = null)
        {
            InitializeComponent();
            this.ContentTitle.Content = title;
            this.FuncoesPesquisa = Pesquisa;
            cmbEncontrar.ItemsSource = Pesquisa.buscaPor;
            cmbEncontrar.SelectedIndex = 0;
            cmbCriterio.ItemsSource = Pesquisa.criterio;
            cmbCriterio.SelectedIndex = 0;
        }

        private void CloseMethod(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Todos(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            String nome = FuncoesPesquisa.table;
            DataTable lista = FuncoesPesquisa.Todos(FuncoesPesquisa.table, Properties.Settings.Default.Connection);
            this.ListItem.ItemsSource = lista.AsDataView();
            this.TotalRecords.Content = lista.AsDataView().Count;
            StatementDataTable();
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void Buscar(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            String nome = FuncoesPesquisa.table;
            int index = this.cmbEncontrar.SelectedIndex;
            int criterio = this.cmbCriterio.SelectedIndex;
            string busca = this.Busca.Text;
            DataTable lista = FuncoesPesquisa.Buscar(FuncoesPesquisa.table, cmbEncontrar.SelectedIndex, cmbCriterio.SelectedIndex, busca, Properties.Settings.Default.Connection);
            this.ListItem.ItemsSource = lista.AsDataView();
            this.TotalRecords.Content = lista.AsDataView().Count;
            StatementDataTable();
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void StatementDataTable()
        {
            Array colums = FuncoesPesquisa.tableCollumName;
            string[] headerNames = FuncoesPesquisa.headerCollumName;
            int index = 0;
            for (int i = 0; i < ListItem.Columns.Count; i++)
            {
                if (Array.IndexOf(colums, this.ListItem.Columns[i].Header.ToString().ToUpper()) < 0)
                {
                    this.ListItem.Columns[i].Visibility = Visibility.Hidden;
                }
                else
                {
                    this.ListItem.Columns[i].Header = headerNames[index];
                    index++;
                }
            }
        }

        private void ListItem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView dataRow = (DataRowView)ListItem.SelectedItem;
            values = new string[dataRow.Row.ItemArray.Length];
            for(int i = 0; i < dataRow.Row.ItemArray.Length; i++)
            {
                values[i] = dataRow.Row.ItemArray[i].ToString();
            }
            this.Close();
        }
    }
}
