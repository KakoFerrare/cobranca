﻿using Cobranca.ModelView;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cobranca.View.Cadastramento
{
    /// <summary>
    /// Interaction logic for Boleto.xaml
    /// </summary>
    public partial class Boleto : UserControl
    {
        ICollection<System.ComponentModel.DataAnnotations.ValidationResult> results = null;
        private MVBoleto mvBoleto;

        public Boleto(MVBoleto mvBoleto)
        {
            InitializeComponent();
            this.mvBoleto = mvBoleto;
            this.DataContext = mvBoleto;
        }

        private void PesquisarConta(object sender, RoutedEventArgs e)
        {
            Button botao = (sender as Button);
            String title = botao.Tag.ToString();
            Funcoes.CN.Pesquisa FuncoesPesquisa = new Funcoes.CN.Pesquisa();
            FuncoesPesquisa.ContaCorrente();
            Pesquisa pesquisa = new Pesquisa(title, FuncoesPesquisa);
            pesquisa.ShowDialog();
            if (!Object.ReferenceEquals(pesquisa.values, null))
            {
                this.Conta.Text = new Funcoes.CN.FuncoesGerais().FormatConta(pesquisa.values[0]);
                this.Nome.Content = pesquisa.values[2];
            }

        }
      
        private void Conta_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            TextBox text = (sender as TextBox);
            text.Text = new Funcoes.CN.FuncoesGerais().FormatZerosEsquerda(text.Text.Replace("-", ""), 10);
            mvBoleto.contaCorrente = new ClassModel.CN.CNContaCorrente().Carregar(text.Text, Properties.Settings.Default.Connection);
            this.Conta.Text = new Funcoes.CN.FuncoesGerais().FormatConta(mvBoleto.contaCorrente.IdConta);
            this.Nome.Content = mvBoleto.contaCorrente.Nome1_Cheque;
        }

        private void ComboBox_DropDownChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox ComboItem = (ComboBox)sender;

            if (ComboItem.SelectedValue.ToString() == "")
            {
                Vencimento.Visibility = Visibility.Visible;
            }
            else
            {
                Vencimento.Visibility = Visibility.Hidden;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            if(Validator.TryValidateObject(mvBoleto.boleto, new ValidationContext(mvBoleto.boleto), results, true))
            {

            }
        }
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
           if(DescontoDia.IsChecked == false && DescontoFixo.IsChecked == false)
            {
                lblDescontoDia.Visibility = Visibility.Hidden;
                DataDescontoDia.Visibility = Visibility.Hidden;
                ValorDescontoDia.Visibility = Visibility.Hidden;
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            ValorDescontoDia.Visibility = Visibility.Visible;
            CheckBox check = (sender as CheckBox);
            if (Convert.ToInt32(check.Tag) == 1)
            {
                DescontoDia.IsChecked = false;
                lblDescontoDia.Visibility = Visibility.Hidden;
                DataDescontoDia.Visibility = Visibility.Hidden;
            }
            else
            {
                DescontoFixo.IsChecked = false;
                lblDescontoDia.Visibility = Visibility.Visible;
                DataDescontoDia.Visibility = Visibility.Visible;
            }
        }
    }
}
