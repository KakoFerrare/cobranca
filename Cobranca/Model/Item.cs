﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Model
{
    public class Item
    {
        public int IdGrupo { get; set; }
        public int IdLista { get; set; }
        public string Descricao { get; set; }
        public int Idusuario_inc { get; set; }
        public DateTime Dt_inc { get; set; }
        public int Idusuario_alt { get; set; }
        public DateTime Dt_alt { get; set; }

    }
}
