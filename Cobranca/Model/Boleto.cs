﻿using ClassModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Model
{
    //https://stackoverflow.com/questions/26527818/wpf-mvvm-validation-using-data-annotation
    //https://www.c-sharpcorner.com/UploadFile/20c06b/screen-validation-with-data-annotations-in-wpf/
    [PropertyChanged.ImplementPropertyChanged]
    public class Boleto : PropertyValidateModel
    {
        
        public string IdConta { get; set; }
        //public ContaCorrente contaCorrente { get; set; }
      
        public string NossoNr { get; set; }
        public string Prefixo { get; set; }
        public string DvNossoNr { get; set; }
        public string IdBordero { get; set; }
        [Required(ErrorMessage = "Precisa dessa merda")]
        [StringLength(5)]
        [Display(Name = "Controle")]
        public string ControleCliente { get; set; }
        public string Parcela { get; set; }
        public string Carteira { get; set; }
        public string Cod_Remessa { get; set; }
        public string NrDoc { get; set; }
        public DateTime ? Vencimento { get; set; }
        public Double ? Valor { get; set; }
        public string Especie { get; set; }
        public string Aceite { get; set; }
        public DateTime ? Data { get; set; }
        public string Instr1 { get; set; }
        public string Instr2 { get; set; }
        public Double? Mora { get; set; }
        public Double? Multa { get; set; }
        public Double? Abatimento { get; set; }
        public int ? IdSacado { get; set; }
        public string Sac_IdConta { get; set; }
        public string Sacador { get; set; }
        public string Sacador_IdCpf { get; set; }
        public DateTime ? DtPagamento { get; set; }
        public Double? ValorPago { get; set; }
        public Int16 ? Agencia_Rec { get; set; }
        public Int16 ? Banco_Rec { get; set; }
        public string TipoBaixa { get; set; }
        public string TpVencimento { get; set; }
        public string CodBarras { get; set; }
        public string LinhaDigitavel { get; set; }
        public string Envio { get; set; }
        public string Sac_Tipo { get; set; }
        public string Sac_Nome { get; set; }
        public string Sac_End { get; set; }
        public string Sac_Bai { get; set; }
        public string Sac_Cep { get; set; }
        public string Sac_Cid { get; set; }
        public string Sac_Uf { get; set; }
        public string Sac_Idcpf { get; set; }
        public string Mensagem1 { get; set; }
        public string Mensagem2 { get; set; }
        public string Mensagem3 { get; set; }
        public string Mensagem4 { get; set; }
        public string Mensagem5 { get; set; }
        public DateTime ? Desc_Ate { get; set; }
        public Decimal? Vlr_Desc { get; set; }
        public Int16 ? Prazo { get; set; }
        public string Banco { get; set; }
        public string Enviado_Itau { get; set; }
        public Int16 ? Agencia_Bol { get; set; }
        public DateTime ? Dt_inc { get; set; }
        public Int16 ? Idusuario_alt { get; set; }
        public DateTime ? Dt_alt { get; set; }


        public Boleto()
        {
            this.Data = DateTime.Now;
            this.Carteira = "04";

        }
    }
}
